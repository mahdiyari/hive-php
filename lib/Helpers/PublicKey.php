<?php

namespace Hive\Helpers;

use StephenHill\Base58;
use Elliptic\EC;

class PublicKey {
  public $hexKey;
  public $stringKey;
  public $prefix;

  function __construct($string, $decoded = false, $prefix = 'STM') {
    if (!$decoded) {
      $this->stringKey = $string;
      $this->hexKey = $this->decodePublic($string);
    } else {
      $this->prefix = $prefix;
      $this->stringKey = $this->encodePublic($string, $prefix);
      $this->hexKey = $string;
    }
  }

  function toString() {
    return $this->encodePublic($this->hexKey, $this->prefix);
  }

  /**
   * Verify a hex message
   */
  function verify($message, $signature) {
    $ec = new EC('secp256k1');
    $key = $ec->keyFromPublic($this->hexKey, 'hex');
    $signature = array('r'=>substr($signature, 2, 64), 's'=>substr($signature, 66, 64));
    return $key->verify($message, $signature);
  }

  private function encodePublic($key, $prefix) {
    $base58 = new Base58();
    $checksum = hash('ripemd160', hex2bin($key));
    $tmp = $key.\substr($checksum, 0, 8);
    return $prefix . $base58->encode(\hex2bin($tmp));
  }

  private function decodePublic($encodedKey) {
    $prefix = \substr($encodedKey, 0, 3);
    $encodedKey = \substr($encodedKey, 3);
    $base58 = new Base58();
    $key = $base58->decode($encodedKey);
    $key = \bin2hex(\substr($key, 0, -4));
    $this->prefix = $prefix;
    return $key;
  }
}

?>