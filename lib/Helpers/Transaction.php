<?php

namespace Hive\Helpers;

class Transaction {
  public int $ref_block_num;
  public int $ref_block_prefix;
  public string $expiration;
  public array $extensions;
  public array $operations;
  public array $signatures;

  private $trxId;
  function getTrxId() {
    return $this->trxId;
  }
  function setTrxId($trxId) {
    $this->trxId = $trxId;
  }
}

?>