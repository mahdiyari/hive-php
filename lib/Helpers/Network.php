<?php

namespace Hive\Helpers;

class Network {
  public $rpcNodes;
  public $timeout;
  function __construct(array $rpcNodes, $timeout) {
    $this->rpcNodes = $rpcNodes;
    $this->timeout = $timeout;
  }

  /**
   * Make a jsonrpc call to the Hive RPC nodes
   *
   * @param string $method
   * @param string $params
   * @return void
   */
  function call($method, $params = '[]', $i = 0) {
    $callData = '{"jsonrpc":"2.0", "method":"'.$method.'", "params":'.$params.', "id":1}';
    $callOptions = array(
      'http' => array(
          'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
          'method'  => 'POST',
          'timeout' => $this->timeout,
          'content' => $callData
      )
    );
    $context  = \stream_context_create($callOptions);
    $result = @\file_get_contents($this->rpcNodes[$i] , false, $context);
    if ($result == FALSE) {
      if(\sizeof($this->rpcNodes) - 1 > $i) {
        return $this->call($method, $params, ++$i);
      } else {
        throw new \Exception('Network failed after '. $i. ' attempts');
      }
    } else {
      $json = \json_decode($result, true);
      if (\array_key_exists('result', $json)) return $json['result'];
      else return $json['error'];
    }
  }

  function getDynamicGlobalProperties() {
    return $this->call('condenser_api.get_dynamic_global_properties', '[]');
  }

  function broadcast(string $trx, string $trxId) {
    $result = $this->call('condenser_api.broadcast_transaction', '['.$trx.']');
    if (\is_array($result) && \sizeof($result) == 0) return array('trx_id' => $trxId);
    else return $result;
  }
}


?>