<?php

namespace Hive\Helpers;

use StephenHill\Base58;
use Elliptic\EC;
use Hive\Helpers\Serializer;

class PrivateKey{
  public string $stringKey;
  public string $hexKey;

  function __construct(string $key, $decoded = false) {
    if(!$decoded) {
      $this->stringKey = $key;
      $this->hexKey = $this->decodeKey($key);
    } else {
      $this->hexKey = $key;
      $this->stringKey = $this->encodeKey('80'.$key);
    } 
  }

  function decodeKey(string $key): string {
    $base58 = new Base58();
    $key = $base58->decode($key);
    return bin2hex(substr(substr($key, 0, -4), 1));
  }

  function sign(string $message) {
    $ec = new EC('secp256k1');
    $key = $ec->keyFromPrivate($this->hexKey, 'hex');
    $serializer = new Serializer();
    $attempts = 0;
    do {
      $attempts++;
      $attemptsHex = $serializer->dec2hex($attempts);
      $pers = hash('sha256', hex2bin($message . $attemptsHex));
      $options = array('canonical' => true, 'pers' => $pers);
      $signature = $key->sign($message, $options);
      $r = $signature->r->toString(16);
      $s = $signature->s->toString(16);
      $signatureString = $r.$s;
    } while (!$this->isCanonical(\substr($signatureString, 2)));
    $recovery = $signature->recoveryParam;
    return $serializer->dec2hex($recovery + 31) . $signatureString;
  }

  private function isCanonical($signature) {
    $sig0 = substr($signature, 0, 2);
    $sig1 = substr($signature, 2, 2);
    $sig32 = substr($signature, 64, 2);
    $sig33 = substr($signature, 66, 2);
    if (
      !(hexdec($sig0) & 0x80) &&
      !(hexdec($sig0) == 0 && !(hexdec($sig1) & 0x80)) &&
      !(hexdec($sig32) & 0x80) &&
      !(hexdec($sig32) == 0 && !(hexdec($sig33) & 0x80))
    ) {
      return true;
    } else {
      return false;
    }
  }

  function createPublic() {
    $ec = new EC('secp256k1');
    $key = $ec->keyFromPrivate($this->hexKey, 'hex');
    $public = $key->getPublic();
    return new PublicKey($public->encode('hex', true), true, 'STM');
  }

  function encodeKey(string $key) {
    $hash = hash('sha256', \hex2bin($key));
    $checksum = hash('sha256', \hex2bin($hash));
    $base58 = new Base58();
    return $base58->encode(\hex2bin($key.substr($checksum, 0, 8)));
  }
}

?>