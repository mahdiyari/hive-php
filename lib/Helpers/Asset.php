<?php

namespace Hive\Helpers;

class Asset {
  public $amount;
  public $symbol;

  function __construct(float $amount, string $symbol) {
    $this->amount = $amount;
    $this->symbol = ($symbol == 'HIVE') ? ('STEEM') : ($symbol == 'HBD' ? 'SBD' : $symbol);
  }

  static function fromString($string) {
    $asset = \explode(' ', $string, 2);
    $symbol = $asset[1];
    if (!in_array($symbol, ['STEEM', 'VESTS', 'SBD', 'TESTS', 'TBD', 'HIVE', 'HBD'])) {
      throw new \Exception('Invalid asset symbol: '.$symbol);
    }
    $amount = \floatval($asset[0]);
    if (!\is_finite($amount)) {
      throw new \Exception('Invalid asset amount: '.$asset[0]);
    }
    return new Asset($amount, $symbol);
  }

  function getPrecision() {
    switch ($this->symbol) {
      case 'TESTS':
      case 'TBD':
      case 'STEEM':
      case 'SBD':
      case 'HBD':
      case 'HIVE':
        return 3;
      case 'VESTS':
        return 6;
    }
  }

  function toString() {
    return \number_format($this->amount, $this->getPrecision()).' '.$this->symbol;
  }
}

?>