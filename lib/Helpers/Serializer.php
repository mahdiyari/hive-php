<?php

namespace Hive\Helpers;

use Hive\Helpers\Asset;

class Serializer {

  function dec2hex($int) {
    $hex = \dechex($int);
    if (strlen($hex)%2 != 0) {
      $hex = \str_pad($hex, strlen($hex) + 1, '0', STR_PAD_LEFT);
    }
    return $hex;
  }
  
  function TransactionSerializer(&$buffer, $data) {
    return $this->ObjectSerializer([
      ['ref_block_num', 'UInt16Serializer'],
      ['ref_block_prefix', 'UInt32Serializer'],
      ['expiration', 'DateSerializer'],
      ['operations', ['OperationSerializer']],
      ['extensions', ['StringSerializer']]
    ], $buffer, $data);
  }

  function ObjectSerializer($keySerializers, &$buffer, $data) {
    foreach ($keySerializers as $keys) {
      $serializer = $keys[1];
      $param = $keys[0];
      if(!is_array($serializer)) {
        $this->$serializer($buffer, $data->$param);
      } elseif (sizeof($serializer) == 1) {
        $arrayLength = sizeof($data->$param);
        $this->writeVariant32($buffer, $arrayLength);
        $serializer = $serializer[0];
        foreach($data->$param as $item) {
          $this->$serializer($buffer, $item);
        }
      } else { //flatmap | optional | static
        if($serializer[0] == 'optional') {
          $valueSerializer = $serializer[1];
          if (\property_exists($param, $data)) {
            $this->Int8Serializer($buffer, 1);
            $this->$valueSerializer($buffer, $data->$param);
          } else {
            $this->Int8Serializer($buffer, 0);
          }
        } elseif($serializer[0] == 'static') {
          if (\sizeof($data->$param) > 0) {
            $dataArray = $data->$param[0];
            $this->writeVariant32($buffer, 1);
            $id = $dataArray[0];
            $item = $dataArray[1];
            $this->writeVariant32($buffer, $id);
            $this->ObjectSerializer([$serializer[1]], $buffer, $item);
          } else {
            $this->writeVariant32($buffer, 0);
          }
        } else {
          $arrayLength = sizeof($data->$param);
          $this->writeVariant32($buffer, $arrayLength);
          $keySerializer = $serializer[0];
          $valueSerializer = $serializer[1];
          foreach($data->$param as $item) {
            $this->$keySerializer($buffer, $item[0]);
            $this->$valueSerializer($buffer, $item[1]);
          }
        }
      }
    }
  }

  function OperationSerializer(&$buffer, $data) {
    $serializer = $this->OperationSerializers()[$data[0]];
    $this->writeVariant32($buffer, $serializer[0]);
    $this->ObjectSerializer($serializer[1], $buffer, $data[1]);
  }

  function StringSerializer(&$buffer, $data) {
    $calc = 0;
    $length = strlen($data);
    $length = $length >> 0;
    if ($length < 1 << 7 ) $calc == 1;
    elseif ($length < 1 << 14) $calc = 2;
    elseif ($length < 1 << 21) $calc = 3;
    elseif ($length < 1 << 28) $calc = 4;
    else $calc = 5;
    $this->writeVariant32($buffer, $length);
    $arrayString = \unpack('C*', $data);
    foreach($arrayString as $arrayItem) {
      $buffer = $buffer.$this->dec2hex($arrayItem);
    }
  }

  function DateSerializer(&$buffer, $data) {
    $date = \DateTime::createFromFormat('Y-m-d\TH:i:s', $data);
    $this->UInt32Serializer($buffer, $date->getTimestamp());
  }

  function UInt8Serializer(&$buffer, $data) {
    $offset = $this->dec2hex($data);
    $offset = \str_pad($offset, 2, "0");
    $buffer = $buffer.$offset;
  }
  function UInt16Serializer(&$buffer, $data) {
    $offset = $this->dec2hex($data & 0x00FF).$this->dec2hex(($data & 0xFF00) >> 8);
    $offset = \str_pad($offset, 4, "0");
    $buffer = $buffer.$offset;
  }
  function UInt32Serializer(&$buffer, $data) {
    if($data == '') {
      $data = 0;
    }
    $offset = $this->dec2hex($data & 0xFF).$this->dec2hex(($data >> 8) & 0xFF);
    $offset = $offset.$this->dec2hex(($data >> 16) & 0xFF).$this->dec2hex(($data >> 24) & 0xFF);
    $offset = \str_pad($offset, 8, "0");
    $buffer = $buffer.$offset;
  }
  function UInt64Serializer(&$buffer, $data) {
    $this->UInt32Serializer($buffer, substr(strval($data), 0, 10));
    $this->UInt32Serializer($buffer, substr(strval($data), 10, 10));
  }
  function Int8Serializer(&$buffer, $data) {
    return $this->UInt8Serializer($buffer, $data);
  }
  function Int16Serializer(&$buffer, $data) {
    return $this->UInt16Serializer($buffer, $data);
  }
  function Int32Serializer(&$buffer, $data) {
    return $this->UInt32Serializer($buffer, $data);
  }
  function Int64Serializer(&$buffer, $data) {
    return $this->UInt64Serializer($buffer, $data);
  }
  function writeVariant32(&$buffer, $data) {
    $data >>= 0;
    while ($data >= 0x80) {
      $b = ($data & 0x7f) | 0x80;
      $buffer = $buffer.$this->dec2hex($b);
      $data >>= 7;
    }
    $buffer = $buffer.$this->dec2hex($data);
    return $this->dec2hex($data);
  }

  function AssetSerializer(&$buffer, $data) {
    $asset = Asset::fromString($data);
    $precision = $asset->getPrecision();
    $this->Int64Serializer($buffer, \round($asset->amount * \pow(10, $precision)));
    $this->UInt8Serializer($buffer, $precision);
    for ($i = 0; $i < 7; $i++) {
      $tmp = \substr($asset->symbol, $i, 1);
      $this->UInt8Serializer($buffer, \ord($tmp) | 0);
    }
  }

  function AuthoritySerializer(&$buffer, $data) {
    return $this->ObjectSerializer([
      ['weight_threshold', 'UInt32Serializer'],
      ['account_auths', ['StringSerializer', 'UInt16Serializer']],
      ['key_auths', ['PublicKeySerializer', 'UInt16Serializer']]
    ], $buffer, $data);
  }

  function PublicKeySerializer(&$buffer, $data) {
    if (
      $data == null || $data == '' ||
      (is_string($data) && \substr($data, -39) == '1111111111111111111111111111111114T1Anm')
    ) {
      $buffer = $buffer.\str_repeat('0', 66);
    } else {
      $publicKey = new PublicKey($data);
      $buffer = $buffer.$publicKey->hexKey;
    }
  }

  function VoidSerializer(&$buffer, $data) {
    throw new \Exception('Void can not be serialized');
  }

  function BooleanSerializer(&$buffer, $data) {
    $this->Int8Serializer($buffer, $data ? 1 : 0);
  }

  function PriceSerializer(&$buffer, $data) {
    $this->ObjectSerializer([
      ['base', 'AssetSerializer'],
      ['quote', 'AssetSerializer']
    ], $buffer, $data);
  }

  function ChainPropertiesSerializer(&$buffer, $data) {
    $this->ObjectSerializer([
      ['account_creation_fee', 'AssetSerializer'],
      ['maximum_block_size', 'UInt32Serializer'],
      ['hbd_interest_rate', 'UInt16Serializer']
    ], $buffer, $data);
  }

  function VariableBinarySerializer(&$buffer, $data) {
    $length = \strlen($data);
    if($length % 2 != 0) {
      $data = \substr($string, 0, -1);
      $length = $length - 1;
    }
    $this->writeVariant32($buffer, $length);
    $buffer = $buffer.$data;
  }

  function BeneficiarySerializer(&$buffer, $data) {
    $length = \sizeof($data);
    $this->writeVariant32($buffer, $length);
    foreach($data as $item) {
      $this->ObjectSerializer([['account', 'StringSerializer'], ['weight', 'UInt16Serializer']], $buffer, $item);
    }
  }

  function OperationSerializers() {
    return array(
      'vote' => [0, [
        ['voter', 'StringSerializer'],
        ['author', 'StringSerializer'],
        ['permlink', 'StringSerializer'],
        ['weight', 'Int16Serializer']
      ]],
      'comment' => [1, [
        ['parent_author', 'StringSerializer'],
        ['parent_permlink', 'StringSerializer'],
        ['author', 'StringSerializer'],
        ['permlink', 'StringSerializer'],
        ['title', 'StringSerializer'],
        ['body', 'StringSerializer'],
        ['json_metadata', 'StringSerializer']
      ]],
      'transfer' => [2, [
        ['from', 'StringSerializer'],
        ['to', 'StringSerializer'],
        ['amount', 'AssetSerializer'],
        ['memo', 'StringSerializer']
      ]],
      'transfer_to_vesting' => [3, [
        ['from', 'StringSerializer'],
        ['to', 'StringSerializer'],
        ['amount', 'AssetSerializer']
      ]],
      'withdraw_vesting' => [4, [
        ['account', 'StringSerializer'],
        ['vesting_shares', 'AssetSerializer']
      ]],
      'limit_order_create' => [5, [
        ['owner', 'StringSerializer'],
        ['orderid', 'UInt32Serializer'],
        ['amount_to_sell', 'AssetSerializer'],
        ['min_to_receive', 'AssetSerializer'],
        ['fill_or_kill', 'BooleanSerializer'],
        ['expiration', 'DateSerializer']
      ]],
      'limit_order_cancel' => [6, [
        ['owner', 'StringSerializer'],
        ['orderid', 'UInt32Serializer']
      ]],
      'feed_publish' => [7, [
        ['publisher', 'StringSerializer'],
        ['exchange_rate', 'PriceSerializer']
      ]],
      'convert' => [8, [
        ['owner', 'StringSerializer'],
        ['requestid', 'UInt32Serializer'],
        ['amount', 'AssetSerializer']
      ]],
      'account_create' => [9, [
        ['fee', 'AssetSerializer'],
        ['creator', 'StringSerializer'],
        ['new_account_name', 'StringSerializer'],
        ['owner', 'AuthoritySerializer'],
        ['active', 'AuthoritySerializer'],
        ['posting', 'AuthoritySerializer'],
        ['memo_key', 'PublicKeySerializer'],
        ['json_metadata', 'StringSerializer']
      ]],
      'account_update' => [10, [
        ['account', 'StringSerializer'],
        ['owner', ['optional', 'AuthoritySerializer']],
        ['active', ['optional', 'AuthoritySerializer']],
        ['posting', ['optional', 'AuthoritySerializer']],
        ['memo_key', 'PublicKeySerializer'],
        ['json_metadata', 'StringSerializer']
      ]],
      'witness_update' => [11, [
        ['owner', 'StringSerializer'],
        ['url', 'StringSerializer'],
        ['block_signing_key', 'PublicKeySerializer'],
        ['props', 'ChainPropertiesSerializer'],
        ['fee', 'AssetSerializer']
      ]],
      'account_witness_vote' => [12, [
        ['account', 'StringSerializer'],
        ['witness', 'StringSerializer'],
        ['approve', 'BooleanSerializer']
      ]],
      'account_witness_proxy' => [13, [
        ['account', 'StringSerializer'],
        ['proxy', 'StringSerializer']
      ]],
      'custom' => [15, [
        ['required_auths', ['StringSerializer']],
        ['id', 'UInt16Serializer'],
        ['data', 'VariableBinarySerializer']
      ]],
      'delete_comment' => [17, [
        ['author', 'StringSerializer'],
        ['permlink', 'StringSerializer']
      ]],
      'custom_json' => [18, [
        ['required_auths', ['StringSerializer']],
        ['required_posting_auths', ['StringSerializer']],
        ['id', 'StringSerializer'],
        ['json', 'StringSerializer']
      ]],
      'comment_options' => [19, [
        ['author', 'StringSerializer'],
        ['permlink', 'StringSerializer'],
        ['max_accepted_payout', 'AssetSerializer'],
        ['percent_hbd', 'UInt16Serializer'],
        ['allow_votes', 'BooleanSerializer'],
        ['allow_curation_rewards', 'BooleanSerializer'],
        ['extensions',['static', ['beneficiaries', 'BeneficiarySerializer']]]
      ]],
      'set_withdraw_vesting_route' => [20, [
        ['from_account', 'StringSerializer'],
        ['to_account', 'StringSerializer'],
        ['percent', 'UInt16Serializer'],
        ['auto_vest', 'BooleanSerializer']
      ]],
      'limit_order_create2' => [21, [
        ['owner', 'StringSerializer'],
        ['orderid', 'UInt32Serializer'],
        ['amount_to_sell', 'AssetSerializer'],
        ['fill_or_kill', 'BooleanSerializer'],
        ['exchange_rate', 'PriceSerializer'],
        ['expiration', 'DateSerializer']
      ]],
      'claim_account' => [22, [
        ['creator', 'StringSerializer'],
        ['fee', 'AssetSerializer'],
        ['extensions', ['VoidSerializer']]
      ]],
      'create_claimed_account' => [23, [
        ['creator', 'StringSerializer'],
        ['new_account_name', 'StringSerializer'],
        ['owner', 'AuthoritySerializer'],
        ['active', 'AuthoritySerializer'],
        ['posting', 'AuthoritySerializer'],
        ['memo_key', 'PublicKeySerializer'],
        ['json_metadata', 'StringSerializer'],
        ['extensions', ['VoidSerializer']]
      ]],
      'request_account_recovery' => [24, [
        ['recovery_account', 'StringSerializer'],
        ['account_to_recover', 'StringSerializer'],
        ['new_owner_authority', 'AuthoritySerializer'],
        ['extensions', ['VoidSerializer']]
      ]],
      'recover_account' => [25, [
        ['account_to_recover', 'StringSerializer'],
        ['new_owner_authority', 'AuthoritySerializer'],
        ['recent_owner_authority', 'AuthoritySeria]izer'],
        ['extensions', ['VoidSerializer']]
      ]],
      'change_recovery_account' => [26, [
        ['account_to_recover', 'StringSerializer'],
        ['new_recovery_account', 'StringSerializer'],
        ['extensions', ['VoidSerializer']]
      ]],
      'escrow_transfer' => [27, [
        ['from', 'StringSerializer'],
        ['to', 'StringSerializer'],
        ['agent', 'StringSerializer'],
        ['escrow_id', 'UInt32Serializer'],
        ['hbd_amount', 'AssetSerializer'],
        ['hive_amount', 'AssetSerializer'],
        ['fee', 'AssetSerializer'],
        ['ratification_deadline', 'DateSerializer'],
        ['escrow_expiration', 'DateSerializer'],
        ['json_meta', 'StringSerializer']
      ]],
      'escrow_dispute' => [28, [
        ['from', 'StringSerializer'],
        ['to', 'StringSerializer'],
        ['agent', 'StringSerializer'],
        ['who', 'StringSerializer'],
        ['escrow_id', 'UInt32Serializer']
      ]],
      'escrow_release' => [29, [
        ['from', 'StringSerializer'],
        ['to', 'StringSerializer'],
        ['agent', 'StringSerializer'],
        ['who', 'StringSerializer'],
        ['receiver', 'StringSerializer'],
        ['escrow_id', 'UInt32Serializer'],
        ['hbd_amount', 'AssetSerializer'],
        ['hive_amount', 'AssetSerializer']
      ]],
      'escrow_approve' => [31, [
        ['from', 'StringSerializer'],
        ['to', 'StringSerializer'],
        ['agent', 'StringSerializer'],
        ['who', 'StringSerializer'],
        ['escrow_id', 'UInt32Serializer'],
        ['approve', 'BooleanSerializer']
      ]],
      'transfer_to_savings' => [32, [
        ['from', 'StringSerializer'],
        ['to', 'StringSerializer'],
        ['amount', 'AssetSerializer'],
        ['memo', 'StringSerializer']
      ]],
      'transfer_from_savings' => [33, [
        ['from', 'StringSerializer'],
        ['request_id', 'UInt32Serializer'],
        ['to', 'StringSerializer'],
        ['amount', 'AssetSerializer'],
        ['memo', 'StringSerializer']
      ]],
      'cancel_transfer_from_savings' => [34, [
        ['from', 'StringSerializer'],
        ['request_id', 'UInt32Serializer']
      ]],
      'custom_binary' => [35, [
        ['required_owner_auths', ['StringSerializer']],
        ['required_active_auths', ['StringSerializer']],
        ['required_posting_auths', ['StringSerializer']],
        ['required_auths', ['AuthoritySerializer']],
        ['id', 'StringSerializer'],
        ['data', 'VariableBinarySerializer']
      ]],
      'decline_voting_rights' => [36, [
        ['account', 'StringSerializer'],
        ['decline', 'BooleanSerializer']
      ]],
      'reset_account' => [37, [
        ['reset_account', 'StringSerializer'],
        ['account_to_reset', 'StringSerializer'],
        ['new_owner_authority', 'AuthoritySerializer']
      ]],
      'set_reset_account' => [38, [
        ['account', 'StringSerializer'],
        ['current_reset_account', 'StringSerializer'],
        ['reset_account', 'StringSerializer']
      ]],
      'claim_reward_balance' => [39, [
        ['account', 'StringSerializer'],
        ['reward_hive', 'AssetSerializer'],
        ['reward_hbd', 'AssetSerializer'],
        ['reward_vests', 'AssetSerializer']
      ]],
      'delegate_vesting_shares' => [40, [
        ['delegator', 'StringSerializer'],
        ['delegatee', 'StringSerializer'],
        ['vesting_shares', 'AssetSerializer']
      ]],
      'account_create_with_delegation' => [41,
      [
        ['fee', 'AssetSerializer'],
        ['delegation', 'AssetSerializer'],
        ['creator', 'StringSerializer'],
        ['new_account_name', 'StringSerializer'],
        ['owner', 'AuthoritySerializer'],
        ['active', 'AuthoritySerializer'],
        ['posting', 'AuthoritySerializer'],
        ['memo_key', 'PublicKeySerializer'],
        ['json_metadata', 'StringSerializer'],
        ['extensions', ['VoidSerializer']]
      ]],
      'witness_set_properties' => [42, [
        ['owner', 'StringSerializer'],
        ['props', ['StringSerializer', 'VariableBinarySerializer']],
        ['extensions', ['VoidSerializer']]
      ]],
      'account_update2' => [43, [
        ['account', 'StringSerializer'],
        ['owner', ['optional', 'AuthoritySerializer']],
        ['active', ['optional', 'AuthoritySerializer']],
        ['posting', ['optional', 'AuthoritySerializer']],
        ['memo_key', ['optional', 'PublicKeySerializer']],
        ['json_metadata', 'StringSerializer'],
        ['posting_json_metadata', 'StringSerializer'],
        ['extensions', ['VoidSerializer']]
      ]],
      'create_proposal' => [44, [
        ['creator', 'StringSerializer'],
        ['receiver', 'StringSerializer'],
        ['start_date', 'DateSerializer'],
        ['end_date', 'DateSerializer'],
        ['daily_pay', 'AssetSerializer'],
        ['subject', 'StringSerializer'],
        ['permlink', 'StringSerializer'],
        ['extensions', ['VoidSerializer']]
      ]],
      'update_proposal_votes' => [45, [
        ['voter', 'StringSerializer'],
        ['proposal_ids', ['Int64Serializer']],
        ['approve', 'BooleanSerializer'],
        ['extensions', ['VoidSerializer']]
      ]],
      'remove_proposal' => [46, [
        ['proposal_owner', 'StringSerializer'],
        ['proposal_ids', ['Int64Serializer']],
        ['extensions', ['VoidSerializer']]
      ]],
      'update_proposal' => [47, [
        ['proposal_id', 'UInt64Serializer'],
        ['creator', 'StringSerializer'],
        ['daily_pay', 'AssetSerializer'],
        ['subject', 'StringSerializer'],
        ['permlink', 'StringSerializer'],
        ['extensions',['static', ['end_date', 'DateSerializer']]]
      ]],
      'collateralized_convert' => [48, [
        ['owner', 'StringSerializer'],
        ['requestid', 'UInt32Serializer'],
        ['amount', 'AssetSerializer']
      ]],
      'recurrent_transfer' => [49, [
        ['from', 'StringSerializer'],
        ['to', 'StringSerializer'],
        ['amount', 'AssetSerializer'],
        ['memo', 'StringSerializer'],
        ['recurrence', 'UInt16Serializer'],
        ['executions', 'UInt16Serializer'],
        ['extensions', ['VoidSerializer']]
      ]]
    );
  }
}

?>