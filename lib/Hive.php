<?php
namespace Hive;

use Hive\Helpers\Network;
use Hive\Helpers\Transaction;
use Hive\Helpers\PrivateKey;
use Hive\Helpers\PublicKey;
use Hive\Helpers\Serializer;

/**
 * Hive Plugin
 */
class Hive
{
  public $rpcNodes = [
    'https://api.hive.blog',
    'https://rpc.ausbit.dev',
    'https://rpc.ecency.com',
    'https://api.pharesim.me',
    'https://api.deathwing.me'
  ];
  public $network;
  public $chainId = 'beeab0de00000000000000000000000000000000000000000000000000000000';
  public $timeout = 7;

  /**
   * @param array $options An array (rpcNodes => [''], chainId => '', timeout => int)
   */
  function __construct(array $options = null)
  {
    set_error_handler(function($errno, $errstr, $errfile, $errline) {
      // error was suppressed with the @-operator
      if (0 === error_reporting()) {
          return false;
      }
      throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
    });
    if(is_array($options)) {
      if (array_key_exists('rpcNodes', $options)) {
        $this->rpcNodes = $options['rpcNodes'];
      }
      if (array_key_exists('chainId', $options)) {
        $this->chainId = $options['chainId'];
      }
      if (array_key_exists('timeout', $options)) {
        $this->timeout = $options['timeout'];
      }
    }
    $this->network = new Network($this->rpcNodes, $this->timeout);
  }

  /**
   * Create a transaction with given operations
   *
   * @param array $operations
   * @param int $expiration in seconds
   * @return Transaction
   */
  function createTransaction(array $operations, int $expiration = 60): Transaction {
    $transaction = new Transaction();
    $props = $this->network->getDynamicGlobalProperties();
    $transaction->ref_block_num = $props['head_block_number'] & 0xffff;
    $transaction->ref_block_prefix = unpack('V', hex2bin($props['head_block_id']), 4)[1];
    date_default_timezone_set('UTC');
    $transaction->expiration = date('Y-m-d\TH:i:s', strtotime('+'.$expiration.' Seconds'));
    $transaction->extensions = [];
    $transaction->signatures = [];
    $transaction->operations = $operations;
    return $transaction;
  }
  
  /**
   * Make a PrivateKey instance from Hive private key string
   * 
   * @param string $key owner/active/posting key
   * @return PrivateKey
   */
  function privateKeyFrom(string $key): PrivateKey {
    return new PrivateKey($key);
  }

  function privateKeyFromLogin(string $username, string $password, string $role = 'active'): PrivateKey {
    $seed = $username.$role.$password;
    return new PrivateKey(hash('sha256', $seed), true);
  }

  function publicKeyFrom(string $key): PublicKey {
    return new PublicKey($key);
  }

  function signTransaction(Transaction $trx, PrivateKey $key) {
    $buffer = '';
    $serializer = new Serializer();
    $serializer->TransactionSerializer($buffer, $trx);
    $digest = hash('sha256', \hex2bin($this->chainId.$buffer));
    $trxId = substr(hash('sha256', \hex2bin($buffer)), 0, 40);
    $trx->setTrxId($trxId);
    $signature = $key->sign($digest);
    $trx->signatures = [$signature];
    return $trx;
  }

  function broadcastTransaction(Transaction $trx) {
    $trasnactionJson = json_encode($trx);
    return $this->network->broadcast($trasnactionJson, $trx->getTrxId());
  }

  /**
   * Build, Sign, and Broadcast the transaction
   * @param PrivateKey $key
   * @param string $opName operation name e.g.: "vote"
   * @param array $opParams operation params e.g. for vote: ['voter', 'author', 'permlink', 10000]
   * @return array an array including trx_id on successful broadcast
   */
  function broadcast(PrivateKey $privateKey, string $opName, array $opParams) {
    $serializer = new Serializer();
    $serializers = $serializer->OperationSerializers();
    if(!\array_key_exists($opName, $serializers)) {
      throw new \Exception($opName.' is not a valid operation.');
    }
    $opSerializer = $serializers[$opName][1];
    if(\sizeof($opSerializer) != \sizeof($opParams)) {
      throw new \Exception('Expected '.\sizeof($opSerializer).' params but instead got '.\sizeof($opParams).' params');
    }
    $i = 0;
    $operation = new \stdClass;
    foreach($opSerializer as $opParam) {
      $tmp = $opParam[0];
      $operation->$tmp = $opParams[$i];
      $i++;
    }
    $operation = array($opName, $operation);
    $trx = $this->createTransaction([$operation]);
    $this->signTransaction($trx, $privateKey);
    return $this->broadcastTransaction($trx);
  }

  function call($method, $params) {
    return $this->network->call($method, $params);
  }
}


?>